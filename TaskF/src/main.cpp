/*
  
 University of Zurich
 Department of Informatics
 Visualization and Multimedia Laboratory
 
 Author: Rafael Ballester
 Date: 12.04.2013
 */

#include <iostream>
#include <vector>
#include <set>
#include <map>

typedef std::vector<int> VI; // to store the distances
typedef std::vector<VI> VVI; // to store an adjacency matrix
typedef std::pair<int,int> PI; // to manipulate elements in the priority queue (key = distance, value = node)

VI dijkstra(VVI& graph, int v) {
    //TODO: fill in your code here
    std::vector<std::pair<int, int> > heap;
    heap.push_back(std::make_pair(0, 0));
    std::push_heap(heap.begin(), heap.end());
    std::vector<int> res;
    res.resize(graph.size());
    res[0] = 0;
    
    bool visitedNodes[graph.size()];
    visitedNodes[0] = true;
    for (int i = 1; i<graph.size(); ++i) {
        visitedNodes[i] = false;
    }
    int i = 0;
    int nodeValue = 0;
    while (std::count(visitedNodes, visitedNodes+graph.size(), true) < graph.size()) {
        for (int j = 0; j<graph[i].size(); ++j) {
            if (graph[i][j]) {
                bool set = true;
                for (std::vector<std::pair<int, int> >::iterator it = heap.begin(); it!=heap.end(); ++it) {
                    if (it->first == j) {
                        it->second = it->second > graph[i][j] + nodeValue ? graph[i][j] + nodeValue : it->second;
                        set = false;
                        break;
                    }
                }
                if (set) {
                    heap.push_back(std::make_pair(j, graph[i][j]+nodeValue));
                    std::push_heap(heap.begin(), heap.end());
                    std::sort_heap(heap.begin(), heap.end());
                }
            }
        }
        for (int j = 0; j<heap.size(); ++j) {  bool tmp = visitedNodes[j];
            if (!visitedNodes[heap[j].first]) {
                visitedNodes[heap[j].first] = true;
                i = heap[j].first;
                nodeValue = heap[j].second;
                res[heap[j].first] = heap[j].second;
                heap.erase(heap.begin()+j);
                std::push_heap(heap.begin(), heap.end());
                break;
            }
        }
    }
    
    return res;
}

int main() {
	VVI graph(9,VI(9,0)); // Create a graph with 9 nodes and no edges (empty adjacency matrix)

	// Add the undirected, weighted edges
	graph[0][1] = graph[1][0] = 3;
	graph[0][3] = graph[3][0] = 7;
	graph[1][2] = graph[2][1] = 1;
	graph[2][4] = graph[4][2] = 9;
	graph[3][2] = graph[2][3] = 3;
	graph[3][4] = graph[4][3] = 6;
	graph[3][6] = graph[6][3] = 11;
	graph[5][4] = graph[4][5] = 2;
	graph[6][5] = graph[5][6] = 4;
	graph[6][8] = graph[8][6] = 5;
	graph[7][3] = graph[3][7] = 5;
	graph[7][8] = graph[8][7] = 4;
	graph[8][5] = graph[5][8] = 8;

	// Do the Dijkstra algorithm forthe first node
	VI distances = dijkstra(graph,0);

	// Output should be 0 3 4 7 13 15 18 12 16
	for (int i = 0; i < distances.size(); ++i) {
		std::cout << distances[i] << " ";
	}
	std::cout << std::endl;
}

