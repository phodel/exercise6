/*
  
 University of Zurich
 Department of Informatics
 Visualization and Multimedia Laboratory
 
 Author: Rafael Ballester
 Date: 12.04.2013
 */

#include <iostream>
#include <vector>
#include <set>
#include <queue>

typedef std::vector<int> VI; // to store the distances
typedef std::vector<VI> VVI; // to store an adjacency matrix

std::set<int> exploredNodes; // set of explored nodes (initially empty)

void BFS(VVI& graph, int v) {
    //TODO: fill in your code here
    std::vector<std::pair<int, int> > exploredEdges;
    VVI list;
    VI currentLevel;
    currentLevel.push_back(v);
    list.push_back(currentLevel);
    exploredNodes.insert(v);
    std::cout << v;
    int l = 0;
    while (!list[l].empty()) {
        currentLevel.erase(currentLevel.begin(), currentLevel.end());
        for (int i = 0; i<list[l].size(); ++i) {
            for (int j = 0; j<graph[i].size(); ++j) {
                if (graph[list[l][i]][j]) {
                    bool unexploredEdge = true;
                    for (std::vector< std::pair<int, int> >::iterator k = exploredEdges.begin(); k!=exploredEdges.end() && unexploredEdge; ++k) {
                        if (k->first == list[l][i] && k->second == j) {
                            unexploredEdge = false;
                        }
                    }
                    if (unexploredEdge) {
                        bool unexploredNode = true;
                        for (std::set<int>::iterator k = exploredNodes.begin(); k!=exploredNodes.end() && unexploredNode; ++k) {
                            if (*k == j) {
                                unexploredNode = false;
                            }
                        }
                        if (unexploredNode) {
                            exploredEdges.push_back(std::make_pair(list[l][i], j));
                            exploredNodes.insert(j);
                            std::cout << ", " << j;
                            currentLevel.push_back(j);
                        }
                    }
                }
            }
        }
        ++l;
        list.push_back(currentLevel);
    }
}

int main() {
	VVI graph(9,VI(9,0)); // Create a graph with 9 nodes and no edges (empty adjacency matrix)

	// Add the undirected edges
	graph[0][1] = graph[1][0] = 1;
	graph[0][3] = graph[3][0] = 1;
	graph[1][2] = graph[2][1] = 1;
	graph[2][4] = graph[4][2] = 1;
	graph[3][2] = graph[2][3] = 1;
	graph[3][4] = graph[4][3] = 1;
	graph[3][6] = graph[6][3] = 1;
	graph[5][4] = graph[4][5] = 1;
	graph[6][5] = graph[5][6] = 1;
	graph[6][8] = graph[8][6] = 1;
	graph[7][3] = graph[3][7] = 1;
	graph[7][8] = graph[8][7] = 1;
	graph[8][5] = graph[5][8] = 1;

	// Do the BFS, starting at the first node
	BFS(graph,0);
}

